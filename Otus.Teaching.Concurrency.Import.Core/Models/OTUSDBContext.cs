﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

#nullable disable

namespace Otus.Teaching.Concurrency.Import.Core.Models
{
    public class OTUSDBContext : DbContext
    {
        public OTUSDBContext()
        {
        }

        public OTUSDBContext(DbContextOptions<OTUSDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseSqlServer(
                    "Data Source=desktop-katz;Initial Catalog=OTUSDB;User ID=user;Password=12345678");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>(entity => { entity.Property(e => e.Id).ValueGeneratedNever(); });
        }
    }
}