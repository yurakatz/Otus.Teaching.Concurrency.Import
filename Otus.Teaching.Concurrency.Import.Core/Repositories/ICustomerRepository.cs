using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        Task<Customer> GetCustomerAsync(int id);
        void AddCustomer(Customer customer);
        void AddRangeCustomer(List<Customer> customers);
        Task AddCustomerAsync(Customer customer);
        Task AddRangeCustomerAsync(List<Customer> customers);
        Task SaveAsync();
        void Save();
    }
}