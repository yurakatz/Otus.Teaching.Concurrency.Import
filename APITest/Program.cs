﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace APITest
{
    internal class Program
    {
        private static HttpClient _client;

        private static async Task Main(string[] args)
        {
            try
            {
                _client = new HttpClient {BaseAddress = new Uri("http://localhost:22839/")};
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            char input;
            do
            {
                Console.WriteLine();
                Console.WriteLine("press 1 for get user info");
                Console.WriteLine("press 2 for customer generation");
                Console.WriteLine("press 3 for exit");

                input = Console.ReadKey().KeyChar;


                switch (input)
                {
                    case '1':
                        await GetCustomer();
                        break;
                    case '2':
                        await GenerationTest();
                        break;
                }
            } while (input != '3');
        }

        private static async Task GenerationTest()
        {
            var rand = new Random();
            var randomcustomer = RandomCustomerGenerator.Generate(1);
            randomcustomer[0].Id = rand.Next(2000000);


            Console.Clear();
            Console.WriteLine(randomcustomer[0]);


            var response = await _client.PostAsync("customer",
                new StringContent(JsonSerializer.Serialize(randomcustomer[0]), Encoding.UTF8, "application/json"));

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Console.WriteLine("Customer added");
                return;
            }

            if (response.StatusCode == HttpStatusCode.Conflict)
            {
                Console.WriteLine("Customer already exist");
                return;
            }

            Console.WriteLine(response.StatusCode);
        }

        private static async Task GetCustomer()
        {
            int id;
            Console.Clear();
            Console.WriteLine("Enter Customer ID to get customer information");
            var userinput = Console.ReadLine();
            var result = int.TryParse(userinput, out id);

            if (result == false)
            {
                Console.WriteLine("Invalid customer ID");
                return;
            }

            var response = await _client.GetAsync($"customer/{id}");

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                Console.WriteLine($"Customer with ID:{id} not exist");
                return;
            }

            var detectedcustomer = JsonConvert.DeserializeObject<Customer>(await response.Content.ReadAsStringAsync());

            Console.WriteLine(detectedcustomer);
        }
    }
}