﻿using System;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    internal class Program
    {
        private static string _dataFileName;
        private static int _dataCount = 100;

        private static string _fileType;

        private static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
                return;

            Console.WriteLine($"File Path:{_dataFileName}");
            Console.WriteLine($"Data Count:{_dataCount}");
            Console.WriteLine($"File Type :{_fileType}");

            var generator = GeneratorFactory.GetGenerator(_dataFileName, _dataCount, _fileType);

            generator.Generate();
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                if (args.Length > 2) _fileType = args[2];

                _dataFileName = args[0]; //Path.Combine(DataFileDirectory, $"{args[0]}");
            }
            else
            {
                Console.WriteLine("Data file name without extension is required");
                return false;
            }

            if (args.Length > 1)
                if (!int.TryParse(args[1], out _dataCount))
                {
                    Console.WriteLine("Data must be integer");
                    return false;
                }


            return true;
        }
    }
}