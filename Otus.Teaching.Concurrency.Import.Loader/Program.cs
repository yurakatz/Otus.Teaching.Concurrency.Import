﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core.Models;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    internal class Program
    {
        private static string _dataFilePath = Path.GetTempFileName();

        private static readonly string GeneratorExecutable = Path
            .Combine(AppDomain.CurrentDomain.BaseDirectory, "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe")
            .Replace(@"Otus.Teaching.Concurrency.Import.Loader", @"Otus.Teaching.Concurrency.Import.DataGenerator.App");

        private static async Task Main(string[] args)
        {
            List<Customer> listCustomers;
            var cfg = InitOptions<AppConfig>();

            if (args != null && args.Length == 1)
                _dataFilePath = args[0];

            await using (var context = new OTUSDBContext())
            {
                var dept = context.Customers.ToList();
                context.RemoveRange(dept);
                await context.SaveChangesAsync();
            }


            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");


            try
            {
                GenerateCustomersDataFile(cfg);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"GenerateCustomersDataFile Error {ex.Message} ");
                return;
            }

            try
            {
                listCustomers = await new CsvParser($@"{_dataFilePath} ").ParseAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"CsvParser Error {ex.Message} ");
                return;
            }

            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
                var loader = new ThreadPoolLoader(listCustomers, 20);

                loader.LoadData();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"CustomerLoader Error {ex.Message} ");
                return;
            }

            stopWatch.Stop();
            Console.WriteLine($"LoadData execution time {stopWatch.ElapsedMilliseconds} ms");

            RemoveFile();
            Console.ReadKey();
        }

        private static void RemoveFile()
        {
            if (File.Exists(_dataFilePath))
                File.Delete(_dataFilePath);
        }

        private static void GenerateCustomersDataFile(AppConfig configuration)
        {
            if (configuration.GeneratorRunAsProcess)
            {
                var processStartInfo = new ProcessStartInfo
                {
                    ArgumentList =
                    {
                        _dataFilePath, configuration.CountOfRecordsForGeneration.ToString(),
                        configuration.FileType
                    },
                    FileName = GeneratorExecutable,
                    UseShellExecute = false
                };


                Process.Start(processStartInfo)?.WaitForExit();
            }
            else
            {
                var generatorTest = GeneratorFactory.GetGenerator(_dataFilePath,
                    configuration.CountOfRecordsForGeneration, configuration.FileType);
                generatorTest.Generate();
            }
        }


        private static T InitOptions<T>()
            where T : new()
        {
            var config = InitConfig();
            return config.Get<T>();
        }

        private static IConfigurationRoot InitConfig()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env}.json", true, true)
                .AddEnvironmentVariables();

            return builder.Build();
        }
    }
}