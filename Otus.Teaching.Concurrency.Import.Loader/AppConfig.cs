﻿//using Otus.Teaching.Concurrency.Import.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    internal class AppConfig
    {
        public int CountOfRecordsForGeneration { get; set; }
        public bool GeneratorRunAsProcess { get; set; }
        public string FileType { get; set; }
    }
}