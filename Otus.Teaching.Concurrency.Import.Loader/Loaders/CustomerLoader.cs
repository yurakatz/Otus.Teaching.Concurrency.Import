﻿using System;
using System.Collections.Generic;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Models;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    internal class CustomerLoader : IDataLoader
    {
        public CustomerLoader(List<Customer> sourceList, int threads)
        {
            if (sourceList == null || sourceList.Count == 0)
                throw new ArgumentException("sourceList is empty");

            Customers = sourceList;

            threads = Math.Max(threads, 1);
            threads = Math.Min(sourceList.Count, threads);
            SubListCount = sourceList.Count / threads;
        }

        public static int SubListCount { get; private set; } = 1;
        public static List<Customer> Customers { get; private set; } = new List<Customer>();

        public void LoadData()
        {
            var threads = new List<Thread>();
            foreach (var i in SplitList(Customers))
            {
                var thread = new Thread(() => UploadData(i));
                thread.Start();
                threads.Add(thread);
            }

            threads.WaitAll();
        }

        public static IEnumerable<List<T>> SplitList<T>(List<T> bigList)
        {
            for (var i = 0; i < bigList.Count; i += SubListCount)
                yield return bigList.GetRange(i, Math.Min(SubListCount, bigList.Count - i));
        }

        private void UploadData(List<Customer> data)
        {
            using var dataContext = new OTUSDBContext();
            var repo = new CustomerRepository(dataContext);

            repo.AddRangeCustomer(data);
            repo.Save();
        }
    }

    public static class ThreadExtension
    {
        public static void WaitAll(this IEnumerable<Thread> threads)
        {
            if (threads != null)
                foreach (var thread in threads)
                    thread.Join();
        }
    }
}