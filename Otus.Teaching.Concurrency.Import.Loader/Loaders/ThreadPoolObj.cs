﻿using System.Collections.Generic;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    internal class ThreadPoolObj
    {
        public List<Customer> DataCustomers;

        public ThreadPoolObj()
        {
            WaitHandle = new AutoResetEvent(false);
        }

        public WaitHandle WaitHandle { get; }
    }
}