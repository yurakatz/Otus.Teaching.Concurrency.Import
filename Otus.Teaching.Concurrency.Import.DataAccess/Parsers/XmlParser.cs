﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        public Task<List<Customer>> ParseAsync()
        {
            throw new NotImplementedException();
        }

        public List<Customer> Parse()
        {
            //Parse data
            return new List<Customer>();
        }
    }
}