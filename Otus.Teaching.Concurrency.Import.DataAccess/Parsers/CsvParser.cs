﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>>
    {
        private readonly string _filepath;

        public CsvParser(string filepath)
        {
            _filepath = filepath;
        }


        public async Task<List<Customer>> ParseAsync()
        {
            var returnData = new List<Customer>();

            try
            {
                returnData = await CsvSerializer<Customer>.DeserializeAsync(new FileStream(_filepath,
                    FileMode.Open, FileAccess.Read));
            }
            catch (FileNotFoundException exc)
            {
                Console.WriteLine($"Error! File not found: {_filepath}\nDetails: {exc.Message}");
            }


            return returnData;
        }
    }
}