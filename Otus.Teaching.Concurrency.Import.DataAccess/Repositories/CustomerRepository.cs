using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Models;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        private readonly OTUSDBContext _dbContext;

        public CustomerRepository(OTUSDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Customer> GetCustomerAsync(int id)
        {
            return await _dbContext.Customers.FirstOrDefaultAsync(p => p.Id == id);
        }

        public void AddCustomer(Customer customer)
        {
            _dbContext.Customers.Add(customer);
        }

        public void AddRangeCustomer(List<Customer> customers)
        {
            _dbContext.Customers.AddRange(customers);
        }

        public async Task AddCustomerAsync(Customer customer)
        {
            await _dbContext.Customers.AddAsync(customer);
        }

        public async Task AddRangeCustomerAsync(List<Customer> customers)
        {
            await _dbContext.Customers.AddRangeAsync(customers);
        }

        public async Task SaveAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
    }
}