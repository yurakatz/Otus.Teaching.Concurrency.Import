﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.Core.Models;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerController(OTUSDBContext context)
        {
            _customerRepository = new CustomerRepository(context);
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<Customer>> GetCustomerAsync(int id)
        {
            var customer = await _customerRepository.GetCustomerAsync(id);
            if (customer == null)
                return StatusCode(404, "пользователь не был найден");

            return Ok(customer);
        }


        [HttpPost]
        public async Task<ActionResult<Customer>> AddCustomerAsync([FromBody] Customer newCustomer)
        {
            var customer = await _customerRepository.GetCustomerAsync(newCustomer.Id);

            if (customer != null)
                return StatusCode(409, "Пользователь уже существует!");

            await _customerRepository.AddCustomerAsync(newCustomer);
            await _customerRepository.SaveAsync();

            return Ok(newCustomer);
        }
    }
}