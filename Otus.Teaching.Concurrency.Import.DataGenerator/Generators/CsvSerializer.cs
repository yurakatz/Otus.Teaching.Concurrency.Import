﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public static class CsvSerializer<T> where T : class, new()
    {
        public static void SaveToCsv(List<T> reportData, Stream stream)
        {
            var lines = new List<string>();
            var props = TypeDescriptor.GetProperties(typeof(T)).OfType<PropertyDescriptor>();
            var header = string.Join(",", props.ToList().Select(x => x.Name));
            lines.Add(header);
            var valueLines = reportData.Select(row =>
                string.Join(",", header.Split(',').Select(a => row.GetType().GetProperty(a)?.GetValue(row, null))));
            lines.AddRange(valueLines);


            using TextWriter sWriter = new StreamWriter(stream);
            foreach (var s in lines)
                sWriter.WriteLine(s);
        }


        public static async Task<List<T>> DeserializeAsync(Stream stream)
        {
            var retrieval = new List<T>();

            var properties = new T().GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);


            using (var sr = new StreamReader(stream))
            {
                var line = await sr.ReadLineAsync();
                var header = line.Split(",");

                while (sr.Peek() >= 0)
                {
                    var str = await sr.ReadLineAsync();

                    if (str != null)
                    {
                        var propertySplit = str.Split(",");

                        var newItem = new T();

                        for (var index = 0; index < header.Length; index++)
                        {
                            var t = header[index];
                            var p = properties.SingleOrDefault(x => x.Name == t);

                            if (p == null)
                                throw new NullReferenceException();

                            var value = Convert.ChangeType(propertySplit[index], p.PropertyType);
                            p.SetValue(newItem, value);
                        }

                        retrieval.Add(newItem);
                    }
                }
            }

            return retrieval;
        }
    }
}