using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class XmlGenerator : IDataGenerator
    {
        private readonly int _dataCount;
        private readonly string _fileName;

        public XmlGenerator(string fileName, int dataCount)
        {
            _fileName = fileName + ".xml";
            _dataCount = dataCount;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using var stream = File.Create(_fileName);
            new XmlSerializer(typeof(CustomersList)).Serialize(stream, new CustomersList
            {
                Customers = customers
            });
        }
    }
}