﻿using System.IO;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvDataGenerator : IDataGenerator
    {
        private readonly int _dataCount;
        private readonly string _fileName;

        public CsvDataGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);

            using var stream = File.Create(_fileName);

            CsvSerializer<Customer>.SaveToCsv(customers, stream);
        }
    }
}